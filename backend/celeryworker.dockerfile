FROM registry.gitlab.com/p4perf4ce/truelabfade:base-image-0.1.1a

ARG CONDA_DIR=/opt/conda
ARG ENVNAME=fade

ENV PATH $CONDA_DIR/bin:$PATH
ENV WORKER_DIR MLservices

COPY . /app
ENV PYTHONPATH=/app

ENV C_FORCE_ROOT=1

WORKDIR /app

RUN chmod +x /app/worker-start.sh \
    && echo "source activate $ENVNAME" > ~/.bashrc

CMD ["bash", "/app/worker-start.sh"]