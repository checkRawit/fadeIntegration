# Conda workspace
FROM continuumio/miniconda3:4.7.12

WORKDIR /tmp
COPY environment.yml /tmp/

ARG ENVNAME=fade

# Building
RUN apt-get autoclean \
    && apt-get update \
    && apt-get install -y \
    gcc \
    g++ \
    cmake \
    && apt-get clean \
    && conda env create --name $ENVNAME --file=environment.yml \
    && conda clean --all -f

# Deploy
RUN useradd --create-home user \
    && echo "source activate $ENVNAME" > ~/.bashrc

WORKDIR /home/user/fade
COPY . /home/user/fade
USER user
# RUN git clone https://gitlab.com/p4perf4ce/truelabfade.git

ENV PATH /opt/conda/envs/fade/bin:$PATH

