import os
from time import time
from typing import Tuple
from pathlib import Path

from loguru import logger

import cv2
import numpy as np

from MLservices.services.Age import get_age_class
from MLservices.services.FaceExtract import detect_face
from MLservices.services.restful_caller import prediction_call

from MLservices.config import settings

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def predict_age(image_path) -> Tuple[list, dict]:
    try:
        assert image_path is not None
    except AssertionError:
        logger.error(f"Nope. None not allowed.")
    try: 
        t = time()
        _, _, face, _, pos = detect_face(image_path)
    except Exception as e:
        logger.error(f"Error during face extraction, {type(e).__name__}")
        return [], {}
    try:
        prediction = prediction_call(settings.ageurl, data=face)
        tdelta = time() - t
        logger.info("Inference time: {}".format(tdelta))
        age_class = get_age_class()
        # for i in prediction:
        #     print(i)
        #     print(age_class[np.argmax(i)])
        result = [age_class[np.argmax(p)] for p in prediction]
        res_detail = {
            index: {
                "age_p": {age_class[_index]: str(p) for _index, p in enumerate(p_array)},
                "position": [{"x1": str(box[0]), "y1": str(box[1]), "x2": str(box[2]), "y2": str(box[3])} for box in pos]
            }
            for index, p_array in enumerate(prediction)
        }
        return result, res_detail
    except Exception as e:
        logger.error(f"Given data {face}. Error Occurred.")
        return [], {}

