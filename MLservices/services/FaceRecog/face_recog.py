from pathlib import Path
import pickle

from time import time

from loguru import logger

import cv2
import numpy as np

from MLservices.services.FaceExtract import detect_face
from MLservices.utils import get_output
from MLservices.config import settings
from MLservices.services.restful_caller import prediction_call

CONFIDENT_THRESHOLD: float = 0.6


def listkey(storage_path: Path):
    """
    List all model dir
    """
    return [str(dirname.stem)
            for dirname in storage_path.iterdir()
            if dirname.is_dir()]


def check_confidence(prob) -> bool:
    """
    Check model confidence, lower than threshold means UNKNOWN
    """
    logger.debug(f"MAP | {prob}")
    for p in prob[0]:
        if p > CONFIDENT_THRESHOLD:
            logger.debug(f"Confidence | {p}")
            return True
        logger.debug(f"Confidence | {p}")
    return False


def findCosineDistance(source_representation, test_representation):
    a = np.matmul(np.transpose(source_representation), test_representation)
    b = np.sum(np.multiply(source_representation, source_representation))
    c = np.sum(np.multiply(test_representation, test_representation))
    return 1 - (a / (np.sqrt(b) * np.sqrt(c)))


def face_recognition(image_path: Path,
                     representations_file: Path,
                     output_image_path: Path,
                     max_similar: int = 5):
    image = cv2.imread(str(image_path))
    try:
        t = time()
        num_faces, _, _, faces, pos = detect_face(image_path)
    except Exception as e:
        logger.error(f'Error during face extraction, {type(e).__name__}')
        return 0, {}
    try:
        # recog
        recognized_person = prediction_call(settings.faceurl, data=faces)
        t1 = time() - t
        logger.info("Inference time: {}".format(t1))
        with representations_file.open('rb') as ref:
            _repr = pickle.load(ref)
            [representatives, labels, names] = _repr[0]

        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        faces_dict = dict()
        # Iterate through all faces
        for i in range(len(pos)):
            label_count = dict()
            face_dict = dict()
            # for each label, set score to 0
            for n in names:
                logger.debug(f'< - label {n}: for n in names. - >')
                label_count[Path(n).stem] = 0
            # find distance from each label
            for j in range(len(labels)):
                dist = findCosineDistance(recognized_person[i], representatives[j])
                if dist < 0.4:
                    ans = labels[j]
                    label_count[ans] += 1
            ans = 'UNKNOWN'
            count = max_similar
            for label in label_count:
                if label_count[label] > count:
                    count = label_count[label]
                    ans = label
                face_dict[label] = label_count[label]
            faces_dict[i] = face_dict
            # Render label to image
            # image = get_output(ans, pos[i], image)
            # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        t2 = time() - t
        logger.info("Comparing time: {}".format(t2 - t1))
        logger.info("Total time: {}".format(t2))
    except Exception as e:
        logger.error(f"Given data {data}. Error Occurred {type(e).__name__}")
        return 0, {}
    # cv2.imwrite(str(output_image_path), image)
    # return image, faces_dict
    return 0, faces_dict
