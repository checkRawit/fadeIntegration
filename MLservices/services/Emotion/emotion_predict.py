from time import time
from typing import Tuple
from pathlib import Path

import cv2
import numpy as np
from loguru import logger

from MLservices.config import settings
from MLservices.services.Emotion import (
    get_emotion_class)
from MLservices.services.FaceExtract import detect_face
from MLservices.services.restful_caller import prediction_call


def predict_emotion(image_path: Path) -> Tuple[list, dict]:
    """
    Params:
    image_path: Path to target image
    Return:
    - list of highest probability result
    - dict of probability map
    """
    try:
        assert image_path.is_file()
    except AssertionError:
        logger.error(f"No such a file {str(image_path)}")
    try:
        t = time()
        _, face, _, _, pos = detect_face(str(image_path))
    except Exception as e:
        logger.error(f"Error during face extraction, {type(e).__name__}")
        return [], {}
    try:
        prediction = prediction_call(settings.emotionurl, data=face)
        tdelta = time() - t
        logger.info("Inference time: {}".format(tdelta))
        emotion_class = get_emotion_class()
        # for i in prediction:
        #     print(i)
        #     print(emotion_class[np.argmax(i)])
        result = [emotion_class[np.argmax(p)] for p in prediction]
        res_detail = {
            index: {
                "emotion_p": {emotion_class[_index]: str(p) for _index, p in enumerate(p_array)},
                "position": [{"x1": str(box[0]), "y1": str(box[1]), "x2": str(box[2]), "y2": str(box[3])} for box in pos]
            }
            for index, p_array in enumerate(prediction)
        }
        return result, res_detail
    except Exception as e:
        logger.error(f"Given this {face} Error Ocurred")
        return [], {}
