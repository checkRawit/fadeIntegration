def get_emotion_class():
    emotion_class = {0: 'Uncertain',
                     1: 'angry',
                     2: 'disgusted',
                     3: 'fearful',
                     4: 'happy',
                     5: 'neutral',
                     6: 'sad',
                     7: 'surprised'}
    return emotion_class
